terraform {

  backend "s3" {
    bucket         = "terraform-ci-osahon"
    dynamodb_table = "jjtech-dynamodb"
    key            = "statefile.tfstate"
    region         = "ca-central-1"
    encrypt        = true

  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.14.0"
    }
  }
}

provider "aws" {
  # Configuration options
}