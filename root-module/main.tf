module "vpc" {
  source      = "../child-module/vpc"
  vpc_cidr    = "10.0.0.0/16"
  vpc_tag     = "canada-vpc"
  subnet_az   = "ca-central-1a"
  subnet_tag  = "main-subnet"
  subnet_cidr = "10.0.1.0/24"


}

module "sg" {
  source = "../child-module/security-group"
  vpc_id = module.vpc.vpc_id
  my_tag = "canada-sg"

}


module "instance" {
  source        = "../child-module/ec2-instance"
  instance_type = "t2.micro"
  vpc_subnet    = module.vpc.subnet_id
  ec2-tags      = "canada-instance"

}
